use scraper::{Html, Selector};

#[derive(Debug)]
pub struct Link {
    pub href: String,
    pub text: String,
}

pub fn generate_links(html_doc: &str) -> Vec<Link> {
    let document = Html::parse_document(html_doc);
    let selector = Selector::parse("a").unwrap();
    let mut return_data = Vec::new();

    for element in document.select(&selector) {
        return_data.push(Link {
            href: element.value().attr("href").unwrap_or("").to_string(),
            text: element.text().collect::<Vec<_>>().join(" "),
        });
    }

    return_data
}
