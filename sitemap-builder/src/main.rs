mod parser;

use clap::Parser;
use quick_xml::events::{BytesDecl, BytesEnd, BytesStart, BytesText, Event};
use quick_xml::Writer;
use std::error::Error;
use std::fs::File;
use url::Url;

/// site map builder for user supplied URL
#[derive(Parser, Default)]
struct UserArgs {
    /// website URL to parse
    #[clap(short, long, value_parser)]
    url: String,

    /// parsing depth level for given URL child pages
    #[clap(short, long, value_parser, default_value_t = 2)]
    depth: usize,
}

fn main() -> Result<(), Box<dyn Error>> {
    let u_arg = UserArgs::parse();

    let url_list = bfs_for_website(&u_arg)?;
    write_urls_to_xml(url_list)?;

    Ok(())
}

fn bfs_for_website(commandline_args: &UserArgs) -> Result<Vec<String>, Box<dyn Error>> {
    let mut site_url = &commandline_args.url;

    let base_url = generate_base_url(site_url)?;
    println!("generated base url: {}", base_url);

    let mut url_list = Vec::new();
    let mut current_depth = 1;
    let mut current_parsing_start_index = 0;
    let mut current_parsing_end_index = 1;

    url_list.push(site_url.to_string());

    while current_depth <= commandline_args.depth {
        let mut urls_to_check =
            vec!["".to_string(); current_parsing_end_index - current_parsing_start_index];
        urls_to_check
            .clone_from_slice(&url_list[current_parsing_start_index..current_parsing_end_index]);

        for url_to_visit in urls_to_check.iter() {
            site_url = &url_to_visit;

            println!("loading html for: {}", site_url);
            let body = ureq::get(site_url).call()?.into_string()?;
            let links = parser::generate_links(body.as_str());

            for ln in links {
                if ln.href.is_empty() {
                    continue;
                }

                if ln.href.starts_with("/") {
                    // this if condition is for relative paths that start with "/" and doesn't have base url or domain name
                    // we are removing leading "/" from link since "base_url" contains trailing "/"
                    validate_and_add_url(format!("{}{}", base_url, &ln.href[1..]), &mut url_list);
                } else if ln.href.starts_with("http") {
                    // if there is a http path then we will generate base url and compare it to user provided base url, if they
                    // are not same then we will ignore it since we only want site map for our website
                    let link_base_url = generate_base_url(&ln.href)?;

                    if link_base_url == base_url {
                        validate_and_add_url(ln.href, &mut url_list);
                    }
                }
            }
        }

        current_depth += 1;
        current_parsing_start_index = current_parsing_end_index;
        current_parsing_end_index = url_list.len();
    }

    println!("total # of links: {}", url_list.len());

    Ok(url_list)
}

fn generate_base_url(full_url: &str) -> Result<String, Box<dyn Error>> {
    let actual_url = Url::parse(full_url)?;
    let new_url = Url::parse(&format!(
        "{}://{}",
        actual_url.scheme(),
        actual_url.host_str().unwrap()
    ));

    Ok(new_url?.to_string())
}

fn validate_and_add_url(url_to_add: String, site_urls_list: &mut Vec<String>) {
    if !site_urls_list.contains(&url_to_add) {
        site_urls_list.push(url_to_add);
    }
}

fn write_urls_to_xml(url_list: Vec<String>) -> Result<(), Box<dyn Error>> {
    let file = File::create("test.xml")?;
    let mut writer = Writer::new_with_indent(file, b' ', 4);

    // output "xml" declaration tag
    writer.write_event(Event::Decl(BytesDecl::new(b"1.0", Some(b"UTF-8"), None)))?;

    //creating actual xml for sitemap
    let mut urlset_element = BytesStart::owned(b"urlset".to_vec(), "urlset".len());

    urlset_element.push_attribute(("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9"));
    writer.write_event(Event::Start(urlset_element))?;

    for link in url_list {
        let url_element = BytesStart::owned(b"url".to_vec(), "url".len());
        let loc_element = BytesStart::owned(b"loc".to_vec(), "loc".len());

        writer.write_event(Event::Start(url_element))?;

        writer.write_event(Event::Start(loc_element))?;
        writer.write_event(Event::Text(BytesText::from_plain_str(link.as_str())))?;
        writer.write_event(Event::End(BytesEnd::borrowed(b"loc")))?;

        writer.write_event(Event::End(BytesEnd::borrowed(b"url")))?;
    }

    writer.write_event(Event::End(BytesEnd::borrowed(b"urlset")))?;

    Ok(())
}
